from Model import Model
from utils import getImageSetDirectories, prepareSubmission, readBaselineCPSNR, plotLoss, collateFunction
import os
from DataLoader import AllImageDataset, ImageDataSubset
from torch.utils.data import DataLoader
from DeepNetworks.MSRGRU import MSRGRU
import torch.optim as optim
from torchvision import transforms
from Transforms import ToTensor, MinMaxNormalize
import torch
import random
from sklearn.model_selection import train_test_split
import numpy as np
import torch.nn.functional as F
from torch import nn
import json

# seed all RNGs for reproducibility
np.random.seed(0)
torch.manual_seed(0)

# CuDNN reproducibility options
torch.backends.cudnn.deterministic = True
torch.backends.cudnn.benchmark = False



# read the config file
with open("config.json", "r") as read_file:
    config = json.load(read_file)

# Initialize the network based on the network configuration
network = MSRGRU(config["network"])
model = Model(network)


data_directory = config["paths"]["data_directory"]
baseline_cpsnrs = readBaselineCPSNR(config["paths"]["norm_file"])

train_set_directories = getImageSetDirectories(os.path.join(data_directory, "train"))

# Split data into train and validation
val_proportion = 0.10
train_list, val_list = train_test_split(train_set_directories, test_size=val_proportion, random_state=1, shuffle=True)

# Create Dataloader
transform = transforms.Compose([
        ToTensor(),
        MinMaxNormalize()
    ])
train_dataset = ImageDataSubset(image_set_directories=train_list, config=config["training"], transform=transform)
train_dataloader = DataLoader(train_dataset, batch_size=config["training"]["batch_size"], shuffle=True, num_workers=4)

config["training"]["create_patches"] = False
val_dataset = ImageDataSubset(image_set_directories=val_list, config=config["training"], transform=transform)
val_dataloader = DataLoader(val_dataset, batch_size=config["training"]["batch_size"], shuffle=False, num_workers=4)

# Craete optimizer and loss
learning_rate = config["training"]["lr"]
criterion = nn.MSELoss()
optimizer = optim.Adam(network.parameters(), lr=learning_rate)


torch.cuda.empty_cache()

best_model, train_history, val_history = model.train(train_dataloader, val_dataloader, criterion, optimizer, baseline_cpsnrs, config)