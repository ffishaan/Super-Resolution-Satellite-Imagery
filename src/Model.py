from Evaluator import MSE, SSIM, PSNR, calculateScore
from utils import traditionalUpscaling, saveImage
from Trainer import trainAndGetBestModel
import os
import warnings
import torch
import numpy as np

class Model:
    def __init__(self, model):
        self.model = model

    def train(self, train_dataloader, val_dataloader, criterion, optimizer, baseline_cpsnrs, config):
        """
        :param train_dataloader: pytorch train dataloader
        :param val_dataloader: pytorch validation data loader
        :param criterion: loss function for optimization
        :param optimizer: optimizer
        :param baseline_cpsnrs: baseline cpsnr for validation score calculation
        :param config: Training config file
        :return:
        """
        dataloaders = {'train': train_dataloader, 'val': val_dataloader}
        best_model, train_history, val_history = trainAndGetBestModel(self.model, dataloaders, criterion, optimizer,
                                                                      baseline_cpsnrs, config)

        return best_model, train_history, val_history

    def testBatch(self, low_res_batch, bicubic_upscaled_batch, low_res_status_batch):
        """

        :param low_res_batch: Lower resolution Images in numpy array : shape [batch, channels, height, width]
        :param bicubic_upscaled_batch: Bicubic Upscalled version
        :return: Super Resolution Images in numpy array
        """
        super_res_batch = self.model((low_res_batch, bicubic_upscaled_batch, low_res_status_batch))
        if torch.cuda.is_available():
            super_res_batch = super_res_batch.cpu().numpy()
        else:
            super_res_batch = super_res_batch.numpy()

        return np.squeeze(super_res_batch)

    def loadPreTrainedWeights(self, pre_trained_weights_path):
        """

        :param pre_trained_weights_path: the pretrained weights paths
        :return:
        """
        pretrained_dict = torch.load(pre_trained_weights_path)
        self.model.load_state_dict(pretrained_dict)

    def test(self, data_loader, output_directory, pre_trained_weights_path):
        """
        :param data_loader: pytorch test dataloader
        :param output_directory: output directory where the super resolution images will be saved
        :param pre_trained_weights_path: the pretrained model path will be required when running learned deep model
        :return: Super Resolution Images in numpy array
        """

        self.loadPreTrainedWeights(pre_trained_weights_path)

        for low_res_batch, bicubic_upscaled_batch, low_res_status_batch, _, image_set_names in data_loader:

            super_res_batch = self.model((low_res_batch, bicubic_upscaled_batch, low_res_status_batch))

            for i, super_res in enumerate(super_res_batch):
                with warnings.catch_warnings():
                    warnings.simplefilter("ignore")
                    saveImage(super_res, os.path.join(output_directory, image_set_names[i] + ".png"))

    def evalute(self, data_loader, pre_trained_weights_path):
        """
        :param image_directories: directories in a list where both low and high res images are saved
        :param pre_trained_weights_path: the pretrained model path will be required when running learned deep model
        :return: Performances based on different evaluation metrics

        """

        self.loadPreTrainedWeights(pre_trained_weights_path)

        result = {'mse': 0, 'psnr': 0, 'ssim': 0}
        for low_res_batch, bicubic_upscaled_batch, low_res_status_batch, high_res_batch, _ in data_loader:

            super_res_batch = self.testBatch(low_res_batch, bicubic_upscaled_batch, low_res_status_batch)

            for i, super_res in enumerate(super_res_batch):
                result['mse'] += MSE(sr=super_res, hr=high_res_batch[i])
                result['psnr'] += PSNR(sr=super_res, hr=high_res_batch[i], data_range=1.0)
                result['ssim'] += SSIM(sr=super_res, hr=high_res_batch[i], data_range=1.0, window_size=7,
                                       multi_channel=1)

        result['mse'] /= len(data_loader.dataset)
        result['psnr'] /= len(data_loader.dataset)
        result['ssim'] /= len(data_loader.dataset)

        return result
