from skimage.measure import compare_mse, compare_psnr, compare_ssim
import numpy as np

def MSE(sr, hr):
    """

    :param sr: Super Resolved image in numpy array ~ [0, 1]
    :param hr: Higher Resolution ground truth image in numpy array  ~ [0, 1]
    :return: MSE Error calculated on these two images

    """

    return compare_mse(hr, sr)


def PSNR(sr, hr, data_range):
    """

    :param sr: Super Resolved image in numpy array
    :param hr: Higher Resolution ground truth image in numpy array
    :param data_range: range of data
    :return: PSNR calculated on these two images

    """

    return compare_psnr(hr, sr, data_range=data_range)


def SSIM(sr, hr, data_range=1.0, window_size=7, multi_channel=True):
    """

        :param sr: Super Resolved image in numpy array
        :param hr: Higher Resolution ground truth image in numpy array
        :param data_range: range of data
        :param window_size: Window size
        :param multi_channel: Multiple channels or not
        :return: SSIM calculated on these two images

        """

    return compare_ssim(hr, sr, data_range=data_range, win_size=window_size, multichannel=multi_channel)


def cPSNR(sr_center, hr_patch, status_map_patch):
    """

    :param sr_center: center 378*378 patch of Super Resolved image in numpy array
    :param hr_patch: 378*378 patch of Higher Resolution ground truth image in numpy array
    :param status_map_patch: 378*378 patch of status map of Higher res image, indicating clear pixels by a value of 1
    :return: PSNR modified for brightness and cloud
    """
    eps = 10e-10
    sr_center = sr_center * status_map_patch
    hr_patch = hr_patch * status_map_patch

    brightness_bias = np.sum(hr_patch-sr_center) / np.sum(status_map_patch)

    cMSE = np.sum(np.square(hr_patch - ((sr_center+brightness_bias) * status_map_patch))) / np.sum(status_map_patch)

    return (-10 * np.log10(cMSE+eps))

def patches(im, size, starts):
    result = []
    for start in starts:
        patch = im[start[0]:(start[0] + size[0]),
                   start[1]:(start[1] + size[1])]
        result.append(patch)

    return result


def calculateScore(sr, hr, status_map, norm):
    """
    :param sr: Super Resolved image in numpy array expected to be in range 0~1
    :param hr: Higher Resolution ground truth image in numpy array expected to be in range 0~1
    :param status_map:  status map of Higher res image, indicating clear pixels by a value of 1
    :param norm: cPSNR of a baseline solution which is used to normalize the submissions
    :return: score for the competition for a single super resolved image

    """
    patch_width = 378
    patch_height = 378

    sr_center = patches(sr, (patch_height, patch_width), [(3, 3)])[0]

    m_score = 0

    starts = [(x, y) for x in range(7) for y in range(7)]
    hr_patches = patches(hr, (patch_height, patch_width), starts)
    status_map_patches = patches(status_map, (patch_height, patch_width), starts)

    max_cpsnr = -1000000
    for i in range(len(hr_patches)):
        cpsnr = cPSNR(sr_center, hr_patches[i], status_map_patches[i])
        if cpsnr > max_cpsnr:
            max_cpsnr = cpsnr

    m_score = norm / max_cpsnr

    return m_score


