import time
import torch
import copy
import numpy as np
from Evaluator import calculateScore
from torch.optim import lr_scheduler
from utils import plot_grad_flow
import torch.nn.functional as F
from torch.autograd import Variable


def calculateEdgeMagnitude(x):
    mask_x = torch.FloatTensor([[1, 0, -1], [2, 0, -2], [1, 0, -1]])
    mask_y = torch.FloatTensor([[1, 2, 1], [0, 0, 0], [-1, -2, -1]])
    #     mask_x = mask_x.view(1, 1, 3, 3).repeat(1, 1, 1, 1)
    mask_x = mask_x.view(1, 1, 3, 3)
    mask_y = mask_y.view(1, 1, 3, 3)

    if torch.cuda.is_available():
        mask_x = mask_x.cuda()
        mask_y = mask_y.cuda()

    grad_x = F.conv2d(x, mask_x, padding=1)
    grad_y = F.conv2d(x, mask_y, padding=1)

    return (torch.abs(grad_x) + torch.abs(grad_y)) / 2


def weighted_mse_loss(input, target, weights):
    out = (input - target) ** 2
    out = out * weights  # .expand_as(out)
    loss = out.mean()
    return loss


def trainAndGetBestModel(model, dataloaders, criterion, optimizer, baseline_cpsnrs, config):
    epoch_start = config["training"]["epoch_start"]
    num_epochs = config["training"]["num_epochs"]
    last_checkpoint_path = config["training"]["checkpoint_path"]

    log_file_path = config["paths"]["log_file_path"]
    checkpoint_dir = config["paths"]["checkpoint_dir"]

    if epoch_start > 0:
        pretrained_dict = torch.load(last_checkpoint_path)
        model.load_state_dict(pretrained_dict)


    start = time.time()
    val_history = {'mse': [], 'score': []}
    train_history = {'mse': []}

    best_model_weights = copy.deepcopy(model.state_dict())
    best_mse_loss = 100
    alpha = 0.5

    if torch.cuda.is_available():
        model.cuda()
        criterion.cuda()

    log_file = open(log_file_path, "a")

    scheduler = lr_scheduler.ReduceLROnPlateau(optimizer, mode='min', factor=0.5, verbose=True, patience=5)
    for epoch in range(epoch_start, epoch_start + num_epochs):
        print('Epoch {}/{}'.format(epoch, num_epochs - 1))
        print('-' * 10)

        log_file.write('Epoch {}/{}\n'.format(epoch, num_epochs - 1))
        log_file.write('-' * 10)

        # Each epoch has a training and validation phase
        for phase in ['train', 'val']:
            if phase == 'train':
                model.train()  # Set model to training mode
            else:
                model.eval()  # Set model to evaluate mode

            running_loss = 0.0
            running_score = 0.0
            # Iterate over data.
            for low_res_batch, bicubic_upscaled_batch, low_res_status_batch, high_res_batch, \
                high_res_status_batch, lr_lengths, image_set_names in dataloaders[phase]:

                if torch.cuda.is_available():

                    # for i, out in enumerate(low_res_batch):
                    #     low_res_batch[i] = low_res_batch[i].cuda()
                    #     bicubic_upscaled_batch[i] = bicubic_upscaled_batch[i].cuda()
                    #     low_res_status_batch[i] = low_res_status_batch[i].cuda()

                    low_res_batch = low_res_batch.cuda()
                    bicubic_upscaled_batch = bicubic_upscaled_batch.cuda()
                    low_res_status_batch = low_res_status_batch.cuda()
                    high_res_status_batch = high_res_status_batch.cuda()
                    high_res_batch = high_res_batch.cuda()

                # zero the parameter gradients
                optimizer.zero_grad()
                # forward
                # track history if only in train
                with torch.set_grad_enabled(phase == 'train'):
                    low_res_batch = torch.cat([low_res_batch, low_res_status_batch], 2)
                    super_res_batch = model(
                        (low_res_batch, bicubic_upscaled_batch, low_res_status_batch, high_res_status_batch))

                    loss = criterion(super_res_batch, high_res_batch)
                    # loss = weighted_mse_loss(super_res_batch, high_res_batch, high_res_status_batch)

                    #                     mag_hr = calculateEdgeMagnitude(high_res_batch)
                    #                     mag_sr = calculateEdgeMagnitude(super_res_batch)
                    #                     print(torch.max(mag_hr))
                    #                     print(torch.max(mag_sr))
                    #                     loss2 = criterion2(mag_hr, mag_sr)
                    #                     weights = torch.abs(mag_hr - mag_sr)
                    #                     loss2 = weighted_mse_loss(super_res_batch, high_res_batch, weights)

                    #                     loss = (1-alpha) * loss1 + alpha * loss2

                    # backward + optimize only if in training phase
                    if phase == 'train':
                        loss.backward()
                        #                         torch.nn.utils.clip_grad_norm_(model.parameters(), 0.1)
                        optimizer.step()
                #                         plot_grad_flow(model.named_parameters())


                running_loss += loss.item() * len(low_res_batch)
                if phase == 'val':
                    super_res_batch = np.squeeze(super_res_batch.cpu().numpy(), axis=1)
                    high_res_batch = np.squeeze(high_res_batch.cpu().numpy(), axis=1)
                    high_res_status_batch = np.squeeze(high_res_status_batch.cpu().numpy(), axis=1)

                    batch_size, _, _ = super_res_batch.shape
                    for i in range(batch_size):
                        sr = super_res_batch[i]
                        hr = high_res_batch[i]
                        status_map = high_res_status_batch[i]

                        norm = baseline_cpsnrs[image_set_names[i]]
                        running_score += calculateScore(sr, hr, status_map, norm)

            epoch_mse_loss = running_loss / len(dataloaders[phase].dataset)
            print('{} Loss: {:.8f}'.format(phase, epoch_mse_loss))
            log_file.write('{} Loss: {:.8f}\n'.format(phase, epoch_mse_loss))
            if phase == 'val':
                epoch_score = running_score / len(dataloaders[phase].dataset)
                print('{} score: {:.8f}'.format(phase, epoch_score))
                log_file.write('{} score: {:.8f}\n'.format(phase, epoch_score))
                val_history['mse'].append(epoch_mse_loss)
                val_history['score'].append(epoch_score)
                scheduler.step(epoch_mse_loss)
            else:
                train_history['mse'].append(epoch_mse_loss)

            # deep copy the model
            if phase == 'val' and epoch_mse_loss < best_mse_loss:
                best_mse_loss = epoch_mse_loss
                best_model_weights = copy.deepcopy(model.state_dict())
        if epoch % 10 == 0:
            torch.save(model.state_dict(), checkpoint_dir + '/epoch_{}.pth'.format(epoch))
        print()

    end = time.time()
    time_elapsed = end - start
    print('Training complete in {:.0f}m {:.0f}s'.format(time_elapsed // 60, time_elapsed % 60))
    print('Best MSE loss: {:4f}'.format(best_mse_loss))

    log_file.write('Training complete in {:.0f}m {:.0f}s\n'.format(time_elapsed // 60, time_elapsed % 60))
    log_file.write('Best MSE loss: {:4f}\n'.format(best_mse_loss))
    log_file.close()

    # load best model weights
    model.load_state_dict(best_model_weights)

    return model, train_history, val_history