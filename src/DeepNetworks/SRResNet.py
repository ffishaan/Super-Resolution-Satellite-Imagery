from torch import nn
import torch


class UpScaleBlock(nn.Module):
    """
        Rearranges elements in a Tensor of shape (_, C*r^2, H, W) to a tensor of shape (_, C, rH, rW). Where, C is the
        input channel size, r is the upscale factor and H and W are the Height and Width respectively.
        This is useful for implementing efficient sub-pixel convolution with a stride of 1/r.
        This idea was introduced in the paper: Real-Time Single Image and Video Super-Resolution Using an
        Efficient Sub-Pixel Convolutional Neural Network (https://arxiv.org/pdf/1609.05158.pdf)
    """
    def __init__(self, num_in_channels, upscale_factor):
        super(UpScaleBlock, self).__init__()
        self.upscale = nn.Sequential(
            nn.Conv2d(in_channels=num_in_channels, out_channels=num_in_channels * upscale_factor**2, kernel_size=3, padding=1),
            nn.PixelShuffle(upscale_factor=upscale_factor),
            nn.PReLU()
        )

    def forward(self, x):
        return self.upscale(x)


class ResidualBlock(nn.Module):
    """
        This class is for creating residual block which is a building block of Super Resolution Residual Network(SRResNet).
        Each residual block consists of two convolutional layers with 3*3 kernel and 64 feature maps followed by
        Batch Normalization and ParametricReLU.
    """
    def __init__(self, num_channels=64):
        super(ResidualBlock, self).__init__()
        self.block = nn.Sequential(
            nn.Conv2d(in_channels=num_channels, out_channels=num_channels, kernel_size=3, padding=1),
            nn.BatchNorm2d(num_features=num_channels),
            nn.PReLU(),

            nn.Conv2d(in_channels=num_channels, out_channels=num_channels, kernel_size=3, padding=1),
            nn.BatchNorm2d(num_features=num_channels),
            nn.PReLU()
        )

    def forward(self, x):
        residual = self.block(x)

        return x + residual


class SRResNet(nn.Module):
    """
        This network is designed to create super resolution images based on upscaling factor. It consists of several
        residual blocks with identical layout. The architecture is proposed in "Photo-Realistic Single Image
        Super-Resolution Using a Generative Adversarial Network (https://arxiv.org/abs/1609.04802). To increase the
        resolution of the input image, they used a subpixel convolution technique proposed in "Real-Time Single Image and
        Video Super-Resolution Using an Efficient Sub-Pixel Convolutional Neural Network (https://arxiv.org/abs/1609.05158).
    """
    def __init__(self, in_channels, out_channels, upscale_factor=3):
        super(SRResNet, self).__init__()
        self.block1 = nn.Sequential(
            nn.Conv2d(in_channels=in_channels, out_channels=64, kernel_size=9, stride=1, padding=4),
            nn.PReLU()
        )
        self.block2 = ResidualBlock(num_channels=64)
        self.block3 = ResidualBlock(num_channels=64)
        self.block4 = ResidualBlock(num_channels=64)
        self.block5 = ResidualBlock(num_channels=64)
        self.block6 = ResidualBlock(num_channels=64)
        self.block7 = nn.Sequential(
            nn.Conv2d(in_channels=64, out_channels=64, kernel_size=3, stride=1, padding=1),
            nn.BatchNorm2d(64)
        )
        block8 = [UpScaleBlock(num_in_channels=64, upscale_factor=upscale_factor)]
        self.block8 = nn.Sequential(*block8)
        self.block9 = nn.Sequential(
            nn.Conv2d(in_channels=64, out_channels=out_channels, kernel_size=9, stride=1, padding=4)
        )

    def forward(self, x):
        low_res_batch, bicubic_upscaled_batch, low_res_status_batch = x
        block1 = self.block1(low_res_batch)
        block2 = self.block2(block1)
        block3 = self.block3(block2)
        block4 = self.block4(block3)
        block5 = self.block5(block4)
        block6 = self.block6(block5)
        block7 = self.block7(block6)
        block8 = self.block8(block1 + block7)
        block9 = self.block9(block8)

        # Converting the ouput to the range 0~1 after using tanh activation
        output = (torch.tanh(block9) + 1) / 2

        return output
