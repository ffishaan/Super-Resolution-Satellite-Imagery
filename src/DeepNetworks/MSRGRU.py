import torch.nn as nn
from torch.autograd import Variable
import torch


class ConvGRUUnit(nn.Module):
    def __init__(self, in_channels, hidden_channels, kernel_size):
        super(ConvGRUUnit, self).__init__()
        padding = kernel_size // 2
        self.in_channels = in_channels
        self.hidden_channels = hidden_channels
        self.reset_gate = nn.Conv2d(in_channels + hidden_channels, hidden_channels, kernel_size, padding=padding)
        self.update_gate = nn.Conv2d(in_channels + hidden_channels, hidden_channels, kernel_size, padding=padding)
        self.out_gate = nn.Conv2d(in_channels + hidden_channels, hidden_channels, kernel_size, padding=padding)

    def forward(self, x, prev_state):

        # get batch and spatial sizes
        batch_size = x.data.size()[0]
        spatial_size = x.data.size()[2:]

        # generate empty prev_state, if it is None
        if prev_state is None:
            state_size = [batch_size, self.hidden_channels] + list(spatial_size)
            if torch.cuda.is_available():
                prev_state = Variable(torch.zeros(state_size)).cuda()
            else:
                prev_state = Variable(torch.zeros(state_size))

        # data size is [batch, channel, height, width]
        stacked_inputs = torch.cat([x, prev_state], dim=1)
        update = torch.sigmoid(self.update_gate(stacked_inputs))
        reset = torch.sigmoid(self.reset_gate(stacked_inputs))
        out_inputs = torch.tanh(self.out_gate(torch.cat([x, prev_state * reset], dim=1)))
        new_state = prev_state * (1 - update) + out_inputs * update

        return new_state


class ConvGRU(nn.Module):
    def __init__(self, config):
        super(ConvGRU, self).__init__()

        self.input_channels = config["in_channels"]
        self.num_layers = config["num_layers"]
        hidden_channels = config["channel_sizes"]
        kernel_sizes = config["kernel_sizes"]

        gru_units = []
        for i in range(0, self.num_layers):
            cur_input_dim = self.input_channels if i == 0 else hidden_channels[i - 1]
            gru_unit = ConvGRUUnit(in_channels=cur_input_dim, hidden_channels=hidden_channels[i], kernel_size=kernel_sizes[i])
            gru_units.append(gru_unit)
        self.gru_units = nn.ModuleList(gru_units)

    def forward(self, x, h=None):

        if h is None:
            hidden_states = [None] * self.num_layers
        num_low_res = x.size(1)
        cur_layer_input = x

        for l in range(self.num_layers):
            gru_unit = self.gru_units[l]
            h = hidden_states[l]

            out = []
            for t in range(num_low_res):
                h = gru_unit(cur_layer_input[:, t, :, :, :], h)
                out.append(h)

            out = torch.stack(out, dim=1)
            cur_layer_input = out

        return cur_layer_input[:, -1, :, :, :], cur_layer_input


class ResidualBlock(nn.Module):
    def __init__(self, channel_size=64, kernel_size=3):
        super(ResidualBlock, self).__init__()
        padding = kernel_size // 2
        self.block = nn.Sequential(
            nn.Conv2d(in_channels=channel_size, out_channels=channel_size, kernel_size=kernel_size, padding=padding),
            # nn.BatchNorm2d(num_features=channel_size),
            nn.PReLU(),

            nn.Conv2d(in_channels=channel_size, out_channels=channel_size, kernel_size=kernel_size, padding=padding),
            # nn.BatchNorm2d(num_features=channel_size),
            nn.PReLU()
        )

    def forward(self, x):
        residual = self.block(x)

        return x + residual


class Encoder(nn.Module):
    def __init__(self, config):
        super(Encoder, self).__init__()
        in_channels = config["in_channels"]
        num_layers = config["num_layers"]
        kernel_size = config["kernel_size"]
        channel_size = config["channel_size"]
        padding = kernel_size // 2

        self.init_layer = nn.Sequential(
            nn.Conv2d(in_channels=in_channels, out_channels=channel_size, kernel_size=kernel_size, padding=padding),
            nn.PReLU())

        res_layers = [ResidualBlock(channel_size, kernel_size) for _ in range(num_layers)]
        self.res_layers = nn.Sequential(*res_layers)

        self.final = nn.Sequential(
            nn.Conv2d(in_channels=channel_size, out_channels=channel_size, kernel_size=kernel_size, padding=padding),
            # nn.BatchNorm2d(channel_size)
        )

    def forward(self, x):
        x = self.init_layer(x)
        x = self.res_layers(x)
        x = self.final(x)

        return x


class Decoder(nn.Module):
    def __init__(self, config):
        super(Decoder, self).__init__()

        self.deconv = nn.Sequential(nn.ConvTranspose2d(in_channels=config["deconv"]["in_channels"],
                                                       out_channels=config["deconv"]["out_channels"],
                                                       kernel_size=config["deconv"]["kernel_size"],
                                                       stride=config["deconv"]["stride"]),
                                    nn.PReLU())

        self.final = nn.Conv2d(in_channels=config["final"]["in_channels"],
                               out_channels=config["final"]["out_channels"],
                               kernel_size=config["final"]["kernel_size"],
                               padding=config["final"]["kernel_size"]//2)

    def forward(self, x):
        x = self.deconv(x)
        x = self.final(x)

        return x


class MSRGRU(nn.Module):
    def __init__(self, config):
        super(MSRGRU, self).__init__()
        self.encode = Encoder(config["encoder"])
        self.fuse = ConvGRU(config["fusion"])
        self.decode = Decoder(config["decoder"])

    def forward(self, x):
        low_res_batch, _, low_res_status_batch, high_res_status_batch = x

        seq_len = low_res_batch.size(1)
        out = []
        for t in range(seq_len):
            out.append(self.encode(low_res_batch[:, t, :, :, :]))

        out = torch.stack(out, dim=1)

        out, _ = self.fuse(out)
        out = self.decode(out)

        return out