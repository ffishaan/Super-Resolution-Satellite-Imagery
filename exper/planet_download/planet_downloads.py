#!/usr/bin/env python

"""
Planet Images aligned with DeepGlobe

Assuming running from home in singularity shell, which includes the the
superresolution repo under the /superres singularity directory.

singularity shell -B /network/tmp1/sankarak/deepglobe/buildings:data,/network/home/sankarak/Super-Resolution-Satellite-Imagery/:/superres /network/tmp1/sankarak/images/superresolution.sif
for j in {0..500}
do
    python3 /superres/exper/planet_download/planet_downloads.py $PLANET_API_KEY 3 $j
done
"""
import sys
sys.path.append("/home/src/")
import satellite as st
import glob
import argparse
import os

parser = argparse.ArgumentParser()
parser.add_argument("api_key", metavar="k", type=str, help="planet api key")
parser.add_argument("n_page", metavar="n", type=int, help="number of features per page to return")
parser.add_argument("index", metavar="i", type=int, help="deepglobe image index")
args = parser.parse_args()
print(args)

# all paths to Khartoum images
data_dir = "/data/"
glob.glob(data_dir + "AOI_5_Khartoum_Train/PAN/")
paths = glob.glob("{}/AOI_5_Khartoum_Train/PAN/*".format(data_dir))

bbox = st.bbox_tiff(paths[args.index])
aoi = {"type": "Polygon", "coordinates": [bbox + [bbox[0]]]}
pages = st.search_pages(
    aoi,
    api_key=args.api_key,
    start="2018-11-01T00:00:00.000Z",
    end="2018-11-30T00:00:00.000Z",
    max_links=2
)

for i in range(len(pages)):
    n_on_page = len(pages[i]["features"])
    thinning_ix = range(0, n_on_page, n_on_page // args.n_page)

    for j in thinning_ix[:args.n_page]:
        ft = pages[i]["features"][j]
        clip = st.clip_request(bbox, ft["id"], args.api_key)

        output_name = st.download_clip(
            clip,
            args.api_key
        )

        with open("deepglobe_planet.csv", "a+") as f:
            f.write("{}\t{}\n".format(paths[args.index], output_name))
