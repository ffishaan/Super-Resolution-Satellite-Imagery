#!/usr/bin/env python
import os
import os.path
import shutil
import zipfile
import glob


def process_row(hr_path, lr_path, outdir=None):
    """
    Process Matched Planet + DeepGlobe imagery

    Example
    -------
    >>> hr_path = "/Users/krissankaran/Desktop/scratch/hr_tifs/PAN_AOI_5_Khartoum_img812.tif"
    >>> lr_path = "/Users/krissankaran/Desktop/scratch/lr_zips/759820d9-10a1-4728-9c12-e1a7d4acf61f.zip"
    >>> process_row(hr_path, lr_path, "/output/")
    """
    if not outdir:
        outdir = os.getcwd()

    # create directory containing the high res image
    subdir_name = os.path.basename(hr_path).replace(".tif", "")
    subdir = os.path.join(outdir, subdir_name)
    if not os.path.exists(subdir):
        os.mkdir(subdir)
        shutil.copyfile(hr_path, os.path.join(subdir, "hr.tif"))

    # copy over the low res image
    output_zip = os.path.join(subdir, "lr.zip")
    shutil.copyfile(lr_path, output_zip)

    # unzip and rename everything
    zip_ref = zipfile.ZipFile(output_zip, 'r')
    zip_ref.extractall(subdir)
    num_lr = len(glob.glob(os.path.join(subdir, "lr*tif")))
    zip_ref.close()

    clip_path = glob.glob(os.path.join(subdir, "*Visual_clip*.tif"))[0].replace(".tif", "")
    os.rename(clip_path + ".tif", os.path.join(subdir, "lr{}.tif".format(num_lr)))

    xml_path = glob.glob(os.path.join(subdir, "*Visual_metadata*"))[0].replace(".tif", "")
    os.rename(xml_path, os.path.join(subdir, "lr{}.xml".format(num_lr)))


if __name__ == "__main__":
    with open("deepglobe_planet.csv", "r") as f:
        for line in f.readlines():
            print(line)
            hr_path, lr_path = line.strip("\n").split("\t")
            try:
                process_row(hr_path, lr_path, "/output/")
            except:
                print("failed to process")
